Developed with Python 3.5

All code files are found in the sample directory. All results are generated in the results directory. In the sample directory, a few auxiliary txt files are found
To execute, run the core.py file.

A full user's manual can be found at http://web.tecnico.ulisboa.pt/ist173393/73393-MEIC-Dissertation.pdf (It may take a few seconds to fully load.) in chapter 6.3 (page 76). Furthermore, in chapter 6.1, each the purpose of each file is explained fully. In the rock_beats directory, all the drum beats that were used are found. In each txt file, the first line gives us the BPM, the volume and the duration in beats of that beat. Each following line corresponds to a drum note, with the following order: Start time, duration in beats and instrument number. The instrument number varies according to the MIDI standard, and can be found here: https://en.wikipedia.org/wiki/General_MIDI#Percussion

Packages that are needed:
MIDIUtil
Numpy
OpenCV
Pillow
Scipy
Scikit-image
SortedContainers

http://www.lfd.uci.edu/~gohlke/pythonlibs/ is a good source of wheels for python libraries (when pip install [lib_name] fails)