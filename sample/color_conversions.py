from skimage import color
import numpy
import time
import math

def rgb_to_hexa (pixels) :
    width = pixels.shape[0]
    height = pixels.shape[1]
    hexa_pixels = numpy.empty([width, height], numpy.dtype(str, 100))
    red = 0
    blue = 0
    green = 0
    print(hexa_pixels[0][0])
    for x in range(0, width) :
        for y in range(0, height) :
            red = hex(pixels[x][y][0]).split('x')[1]
            red = clean_hex_values(red)

            green = hex(pixels[x][y][1]).split('x')[1]
            green = clean_hex_values(green)

            blue = hex(pixels[x][y][2]).split('x')[1]
            blue = clean_hex_values(blue)

            hexa = red + green + blue
            print(hexa)
            hexa_pixels[x][y] = hexa
            print(hexa_pixels[x][y])

    print(hexa_pixels[120][230])
    return hexa_pixels

def convert_rgb(pixels) :
    width = pixels.shape[0]
    height = pixels.shape[1]
    hsl_pixels = numpy.empty([width, height, 3], numpy.int16)
    lab_pixels = numpy.empty([width, height, 3])

    for x in range(0, width):
        for y in range(0, height):
            red = pixels[x][y][0]
            green = pixels[x][y][1]
            blue = pixels[x][y][2]
            hsl = pixel_rgb2hsl(red, green, blue)
            hsl_pixels[x][y] = hsl

            lab = pixel_rgb2lab(red, green, blue)
            lab_pixels[x][y] = lab

    return hsl_pixels, lab_pixels

def rgb2hsl (pixels) :
    width = pixels.shape[0]
    height = pixels.shape[1]
    hsl_pixels = numpy.empty([width, height, 3], numpy.int16)
    initial_time = time.time()
    for x in range(0, width) :
        for y in range(0, height) :
            red = pixels[x][y][0]
            green = pixels[x][y][1]
            blue = pixels[x][y][2]
            hsl = pixel_rgb2hsl(red, green, blue)
            hsl_pixels[x][y] = hsl
    final_time = time.time()
    print("Time to convert to HSL: " + str(final_time - initial_time))
    return hsl_pixels

def pixel_rgb2hsl (r, g, b) :
    r = round(r/255, 2)
    g = round(g/255, 2)
    b = round(b/255, 2)

    maximum = max(r, g, b)
    minimum = min(r, g, b)
    difference = maximum - minimum
    luminance = (maximum+minimum)/2

    if(difference == 0) :
        saturation = 0
        hue = -1
        return numpy.array([int(hue), int(saturation * 100), int(luminance * 100)])
    else :
        saturation = difference/(1 - abs(2 * luminance - 1))

    if (maximum == r) :
        hue = round(((g-b)/difference)%6, 2)
    elif (maximum == g) :
        hue = round(2.0 + (b-r)/difference, 2)
    else :
        hue = round(4.0 + (r-g)/difference, 2)

    hue *= 60
    if (hue < 0) :
        hue += 360
    hue = int(hue)
    saturation = int(saturation * 100)
    luminance = int(luminance * 100)
    return numpy.array([hue, saturation, luminance])

def hsl2rgb (pixels) :
    width = pixels.shape[0]
    height = pixels.shape[1]
    rgb_pixels = numpy.empty([width, height, 3], numpy.int16)

    for x in range(0, width):
        for y in range(0, height):
            hue = pixels[x][y][0]
            saturation = pixels[x][y][1]
            lightness = pixels[x][y][2]
            rgb = pixel_hsl2rgb(hue, saturation, lightness)
            rgb_pixels[x][y] = rgb

    return rgb_pixels

def pixel_hsl2rgb (h, s, l) :
    s = round(s/100, 2)
    l = round(l/100, 2)

    c = (1 - abs(2 * l - 1)) * s
    x = c * (1 - abs((h/60) % 2 - 1))
    m = l - c/2

    if(h >= 0 and h < 60) :
        red = c
        green = x
        blue = 0
    else :
        if (h >= 60 and h < 120) :
            red = x
            green = c
            blue = 0
        else :
            if (h >= 120 and h < 180) :
                red = 0
                green = c
                blue = x
            else :
                if (h >= 180 and h < 240) :
                    red = 0
                    green = x
                    blue = c
                else :
                    if (h >= 240 and h < 300) :
                        red = x
                        green = 0
                        blue = c
                    else :
                        red = c
                        green = 0
                        blue = x

    r = (red + m) * 255
    g = (green + m) * 255
    b = (blue + m) * 255
    return numpy.array([int(r), int(g), int(b)])

def pixel_rgb2lab(r, g, b):

    rgb = oneD_to_threeD(numpy.array([r, g, b]))
    rgb[0][0] = rgb[0][0] / 255
    lab = color.rgb2lab(rgb)
    return threeD_to_oneD(lab)

def pixel_lab2rgb (l, a, b) :
    lab = oneD_to_threeD(numpy.array([l, a, b]))
    rgb = color.lab2rgb(lab)
    result = threeD_to_oneD(rgb) * 255
    return numpy.array([int(round(result[0])),
                        int(round(result[1])),
                        int(round(result[2]))])

def pixel_lab2hsl (l, a, b) :
    rgb = pixel_lab2rgb(l, a, b)
    return pixel_rgb2hsl (rgb[0], rgb[1], rgb[2])

def pixel_hsl2lab (h, s, l) :
    rgb = pixel_hsl2rgb(h, s, l)
    return pixel_rgb2lab(rgb[0], rgb[1], rgb[2])

def pixel_count_rgb2lab (rgb_count) :
    pixel = rgb_count[1]
    count = rgb_count[0]
    lab = pixel_rgb2lab(pixel[0], pixel[1], pixel[2])
    return (count, lab)

def count_rgb2lab (rgb_pixel_count) :
    lab_pixel_count = []
    for i in range (0, len(rgb_pixel_count)) :
        pixel_count_rgb2lab(rgb_pixel_count[i])
    return lab_pixel_count

def srgb2linear_component (c) :
    c /= 255
    a = 0.055
    if (c <= 0.04045) :
        c_linear = c / 12.92
    else :
        c_linear = math.pow(((c + a) / (1 + a)), 2.4)

    return c_linear

def calculate_luminance (rgb) :
    rgb /= 255
    rgbT = oneD_to_threeD(rgb)
    xyzT = color.rgb2xyz(rgbT)
    xyz = threeD_to_oneD(xyzT)
    lum = xyz[1]
    return lum


###### AUX Functions

def clean_hex_values(value) :
    if len(value) == 1 :
        value = '0' + value
    return value

def nparray_to_tuple (array) :
    tup = ()
    for x in range (0, array.shape[0]) :
        tup1 = (array[x],)
        tup += tup1
    return tup

def oneD_to_threeD (array) :
    result = numpy.empty((1, 1, 3))
    for i in range (0, 3) :
        result[0][0][i] = array[i]

    return result

def threeD_to_oneD (array) :
    result = numpy.empty((3))
    for i in range (0, 3) :
        result[i] = array[0][0][i]
    return result

def tuple_to_nparray (tup) :
    result = numpy.empty(len(tup))
    for i in range (0, len(tup)) :
        result[i] = tup[i]

    return result

