from midiutil import MidiFile
import random
import numpy
import time
import glob

RAW = True

BASS_OCTAVE = 24
GUITAR_OCTAVE = 48
NOTES_PER_CHORD = 3
CHORD_OCTAVE = 48

VOL_WEIGHT = 0.25
DEN_WEIGHT = 0.25
TEMPO_WEIGHT = 0.25
DUR_WEIGHT = 0.25

GUITAR_DELAY = 0.02
GUITAR_DURATION = 4/7
NUMBER_CHORDS = 7
CHORD_ORDER = [True, True, False, False, True, False, True]

class Note :

    def __init__ (self, pitch, start_time, duration, volume) :
        self.pitch = pitch
        self.duration = duration
        self.start_time = start_time
        self.volume = volume

    def get_pitch(self):
        return self.pitch

    def get_duration(self):
        return self.duration

    def get_start_time(self):
        return self.start_time

    def get_volume(self):
        return self.volume

    def update_start_time (self, new_time) :
        self.start_time = new_time

    def is_equal(self, note):
        if(self.pitch == note.get_pitch()) :
            if(self.duration == note.get_duration()) :
                if(self.start_time == note.get_start_time()) :
                    if(self.volume == note.get_volume()) :
                        return True
        return False

    def print_note(self, bps):
        duration = self.duration / bps
        print("Pitch: " + str(self.pitch))
        print("Beats: " + str(self.duration))
        print("Seconds: "+ str(duration))
        print("Start time: " + str(self.start_time))
        print("##############################")


class Song :
    def __init__(self, bpm, duration, octave, avg_volume, type):
        self.bpm = bpm
        self.duration = duration
        self.type = type
        if(type == 'raw') :
            self.octave = -1
        else :
            self.octave = octave
        self.time_signature = (4, 4)
        self.notes = []
        self.bass = []
        self.percussion = []
        self.chords = []
        self.total_chords = -1
        self.avg_volume = avg_volume

    def get_bpm(self):
        return self.bpm
    def set_bpm(self, bpm):
        self.bpm = bpm

    def get_type(self):
        return self.type

    def get_duration(self):
        return self.duration

    def set_duration(self, duration) :
        self.duration = duration

    def get_notes(self):
        return self.notes

    def get_time_signature(self):
        return self.time_signature

    def get_avg_volume(self):
        return self.avg_volume

    def set_avg_volume (self, volume) :
        self.avg_volume = volume

    def get_chords(self):
        return self.chords

    def get_octave(self):
        return self.octave

    def set_octave(self, octave):
        self.octave = octave

    def add_chord(self, chord):
        self.chords.append(chord)

    def get_bass (self) :
        return self.bass

    def get_total_chords(self):
        return self.total_chords

    def add_total_chords(self, num):
        self.total_chords += num

    def add_note(self, note):
        self.notes.append(note)

    def add_bass_chord (self, chord) :
        self.bass.append(chord)

    def add_percussion (self, beats):
        self.percussion.append(beats)

    def get_percussion(self):
        return self.percussion

    def print_song(self):
        bps = int(self.bpm / 60)
        for i in range(0, len(self.notes)):
            note = self.notes[i]
            note.print_note(bps)
            print("-----------------------")


class Chord :

    def __init__(self, number, type, count=-1, octave=48, volume=100):
        self.name = number2note(number)
        self.note = number
        if (type == "major") :
            self.is_major = True
        else :
            self.is_major = False
        self.count = count
        self.octave = octave
        self.volume = volume
        self.notes = []
        self.load_notes()

    def copy_chord(self):
        if (self.is_major == True) :
            type = "major"
        else :
            type = "minor"
        new_chord = Chord(self.note, type, self.count, self.octave, self.volume)
        return new_chord

    def get_notes(self):
        return self.notes

    def get_chord(self):
        return self.note

    def get_name(self):
        return self.name

    def get_note(self, i):
        return self.notes[i]

    def get_count(self):
        return self.count

    def load_notes(self):
        self.notes = load_chord(self.note, self.is_major)

    def get_volume(self):
        return self.volume
    def get_octave(self):
        return self.octave
    def major (self) :
        return self.is_major

    def copy_into_chord (self, chord) :
        self.name = chord.get_name()
        self.note = chord.get_chord()
        self.is_major = chord.major()
        self.count = chord.get_count()
        self.octave = chord.get_octave()
        self.volume = chord.get_volume()
        self.notes = chord.get_notes()

    def print(self):
        if (self.is_major) :
            print(self.name  + " Major")
        else :
            print(self.name  + " minor")

        print(self.notes)


class Percussion :
    def __init__(self) :
        self.all_beats = []
        self.load_percussion()

    def load_percussion(self):
        file = open("beats.txt", "w")
        for filename in glob.glob('rock_beats/*.txt'):
            beats = numpy.genfromtxt(filename, dtype='str')
            bpm = int(beats[0][0])
            volume = int(beats[0][1])
            beat_duration = int(beats[0][2])
            beat_lst = []
            total_notes = beats.shape[0] - 1
            sum_durations = 0
            for i in range (1, total_notes+1) :
                beat = beats[i]
                start_time = float(beat[0])
                duration = float(beat[1])
                sum_durations += duration
                pitch = int(beat[2])
                beat_lst.append((start_time, duration, pitch))
            new_beat = Beat(filename, bpm, volume, beat_duration, beat_lst, total_notes, sum_durations)
            file.write("Beat " + filename + "\n")
            file.write("\tEmotional Value: " + str(new_beat.get_emotion()) + "\n\n")

            self.all_beats.append(new_beat)
        file.close()

    def select_beats_random(self):
        while (True):
            first = random.randint(1, 20)
            if (first in self.all_beats):
                break
        second = random.randint(21, 32)
        return (self.all_beats[first], self.all_beats[second])

    def get_random_beat(self) :
        number = random.randint(0, len(self.all_beats)-1)
        return self.all_beats[number]

    def get_beats(self):
        return self.all_beats


class Beat :

    def __init__ (self, name, bpm, volume, beat_duration, beats, total_notes, sum_durations) :
        self.name = name
        self.bpm = bpm
        self.volume = volume
        self.sum_durations = sum_durations
        self.beat_duration = beat_duration
        self.beats = beats
        self.avg_durations = sum_durations / total_notes
        self.density = total_notes / beat_duration
        self.emotion = 0
        self.total_notes = total_notes
        self.calculate_emotion()

    def copy_beat(self):
        new = Beat(self.name, self.bpm, self.volume, self.beat_duration, self.beats, self.total_notes, self.sum_durations)
        return new

    def copy_into_beat(self, beat):
        self.name = beat.get_name()
        self.bpm = beat.get_bpm()
        self.volume = beat.get_volume()
        self.sum_durations = beat.get_sum_durations()
        self.beat_duration = beat.get_beat_duration()
        self.beats = beat.get_beats()
        self.total_notes = beat.get_total_notes()

        self.avg_durations = self.sum_durations / self.total_notes
        self.density = self.total_notes / self.beat_duration
        self.emotion = beat.get_emotion()

    def get_name(self):
        return self.name
    def get_bpm(self):
        return self.bpm
    def get_emotion(self):
        return self.emotion
    def get_volume(self):
        return self.volume
    def get_beat_duration(self):
        return self.beat_duration
    def get_beats(self):
        return self.beats
    def get_total_notes(self):
        return self.total_notes
    def get_sum_durations(self):
        return self.sum_durations
    def set_density(self, density):
        self.density = density
    def set_avg_duration(self, avg):
        self.avg_durations = avg
    def get_beat(self, i):
        if (i < len(self.beats)) :
            return self.beats[i]
        else :
            return None
    def set_volume (self, volume) :
        self.volume = volume

    def calculate_emotion(self) :
        scaled_volume = self.volume * (100/127)
        scaled_density = self.density * 12.5
        scaled_note_duration = self.avg_durations * 25
        scaled_tempo = self.bpm - 50
        emo = VOL_WEIGHT * scaled_volume + DEN_WEIGHT * scaled_density + DUR_WEIGHT * (100 - scaled_note_duration) + TEMPO_WEIGHT * scaled_tempo
        self.emotion = emo


class ChordProgression :

    def __init__ (self, chords) :
        self.chords = chords

    def is_prog(self, chords):
        best_prog = -1
        prog = 0
        for i in range (0, len(chords)) :
            chord = chords[i]
            if (chord in self.chords) :
                prog += 1
            else :
                prog = 0
            if(prog > best_prog) :
                best_prog = prog
        return best_prog


class RockProgressions :

    def __init__(self) :
        self.progressions = []
        prog = [(0, True), (5, True), (7, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(0, True), (9, False), (5, True), (7, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(4, True), (9, True), (11, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(4, True), (1, False), (9, True), (11, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(2, True), (7, True), (9, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(2, True), (11, False), (7, True), (9, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(7, True), (0, True), (2, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(7, True), (4, False), (0, True), (2, True)]
        self.progressions.append(ChordProgression(prog))
        prog = [(4, True), (9, True), (2, True)]
        self.progressions.append(ChordProgression(prog))

        self.rock_chords = [(0, True), (1, False), (2, True), (4, False), (4, True), (5, True), (7, True), (9, False), (9, True), (11, True), (11, False)]

    def is_prog(self, chords) :
        solution = 0
        for i in range (0, len(self.progressions)) :
            prog = self.progressions[i]
            best = prog.is_prog(chords)
            if (best > solution) :
                solution = best
        rock_chords = self.num_chords(chords)
        return (solution, rock_chords)

    def num_chords(self, chords):
        rock_chords = 0
        for i in range (0, len(chords)) :
            if (chords[i] in self.rock_chords) :
                rock_chords += 1
        return rock_chords



def calculate_song_duration (bpm) :
    while(True) :
        dur = random.randint(90, 210)
        beats = int((dur * bpm) / 60)
        if (beats % 4 == 0) :
            break
    return beats

def generate_song (song, filename) :

    midifile = MidiFile.MIDIFile(removeDuplicates=False, adjust_origin=False, deinterleave=False)
    midifile.addTempo(0, 0, song.get_bpm())
    time_in_secs = int(60 * song.get_duration() / song.get_bpm())
    print("Song Duration: " + str(time_in_secs))
    add_piano_notes(midifile, 0, 0, song)
    add_bass_line(midifile, 0, 3, song)
    add_percussion(midifile, 0, 9, song)
    add_chord(midifile, 0, 1, song)
    #add_guitar_chord_2(midifile, 0, 2, song)
    initial_time = time.time()

    filename += "-" + song.get_type()

    create_info_file(song, filename)
    with open("../results/" + filename + ".midi", 'wb') as output_file:
        midifile.writeFile(output_file)

    final_time = time.time()
    print("Time to write to file: " + str(final_time - initial_time) + " seconds")

# Piano notes will be added on channel 0
def add_piano_notes(midifile, track, channel, song):
    bpm = song.get_bpm()
    notes = song.get_notes()
    initial_time = time.time()

    for i in range(0, len(notes)):
        note = notes[i]
        note.print_note(2)
        start_time = note.get_start_time()
        duration_in_beats = note.get_duration()
        pitch = note.get_pitch()
        volume = note.get_volume() + 15
        if (volume > 127) :
            volume = 127
        if (pitch > 127):
            pitch = 127
        midifile.addNote(track, channel, pitch, start_time, duration_in_beats, volume)

    final_time = time.time()
    print("Time to add piano notes: " + str(final_time - initial_time) + " seconds")

# Channel 1, may be in various instruments
def add_chord(midifile, track, channel, song):
    start_time = 0
    chords = song.get_chords()
    #midifile.addProgramChange(track, channel, 0, 29)
    #if (song.get_total_chords() == -1) :
    for i in range (0, len(chords)) :
        chord = chords[i]
        notes = chord.get_notes()
        volume = chord.get_volume() - 10
        if (volume <= 15) :
            volume = 15
        for j in range (0, 4) :
            for k in range (0, len(notes)) :
                note = notes[k] + CHORD_OCTAVE
                midifile.addNote(track, channel, note, start_time+j, 1, volume)
        start_time += 4

# Channel 2, guitar
def add_guitar_chord (midifile, track, channel, song) :
    chords = song.get_chords()
    midifile.addProgramChange(track, channel, 0, 29)
    bpm = song.get_bpm()

    delay = (GUITAR_DELAY * bpm) / 60
    for i in range(0, len(chords)):
        start_time = i * 4
        chord = chords[i]
        note = chord.get_chord()
        is_major = chord.major()
        notes = load_guitar_chords(note, is_major)
        volume = chord.get_volume()
        volume -= 10
        if (volume <= 15) :
            volume = 15
        print(delay)
        for j in range (0, NUMBER_CHORDS) :
            current_order = CHORD_ORDER[j]
            for k in range(0, len(notes)):
                if(current_order) :
                    current_note = notes[k]
                else :
                    current_note = notes[len(notes) - 1 - k]
                midifile.addNote(track, channel, current_note, start_time + (k * delay), GUITAR_DURATION+1.5 - (k * delay), volume)
                print("Start: " + str(start_time + (k * delay)))
                print("duration: " + str(GUITAR_DURATION+1.5 - (k * delay)))
            start_time += GUITAR_DURATION


def add_guitar_chord_2(midifile, track, channel, song):
    start_time = 0
    chords = song.get_chords()
    midifile.addProgramChange(track, channel, 0, 30)

    for i in range(0, len(chords)):
        chord = chords[i]
        note = chord.get_chord()
        is_major = chord.major()
        notes = load_guitar_chords(note, is_major)
        volume = chord.get_volume() - 10
        if (volume <= 15):
            volume = 15
        for j in range(0, 4):
            for k in range(0, len(notes)):
                note = notes[k]
                midifile.addNote(track, channel, note, start_time + j, 1.5, volume)
        start_time += 4

# Channel 3
def add_bass_line(midifile, track, channel, song):
    bpm = song.get_bpm()
    total_duration = song.get_duration()
    chords = song.get_chords()

    midifile.addTempo(track, 0, bpm)
    midifile.addProgramChange(track, channel, 0, 33)
    initial_time = time.time()

    start_time = 0
    number_chords = len(chords)
    for i in range (0, number_chords) :
        chord = chords[i]
        first = chord.get_note(0)
        second = chord.get_note(1)
        third = chord.get_note(2)
        volume = chord.get_volume() - 10
        if (volume <= 15):
            volume = 15
        midifile.addNote(track, channel, first + BASS_OCTAVE, start_time, 1, 80)
        midifile.addNote(track, channel, second + BASS_OCTAVE, start_time+1, 1, 80)
        midifile.addNote(track, channel, third + BASS_OCTAVE, start_time+2, 1, 80)
        midifile.addNote(track, channel, first + BASS_OCTAVE, start_time+3, 1, 80)
        start_time += 4
    final_time = time.time()
    print("Time to add bass line: " + str(final_time - initial_time) + " seconds")

# Channel 9
def add_percussion(midifile, track, channel, song):
    initial_time = time.time()
    total_duration = song.get_duration()
    beats = song.get_percussion()
    current_time = 0
    for i in range (0, len(beats)) :
        beat = beats[i]
        beat_duration = beat.get_beat_duration()
        total_notes = beat.get_total_notes()
        volume = beat.get_volume()
        for j in range (0, total_notes) :
            current = beat.get_beat(j)
            start_time = current[0] + current_time
            if(current[0] >= 4 or start_time > total_duration) :
                continue
            duration = current[1]
            pitch = current[2]
            midifile.addNote(track, channel, pitch, start_time, duration, volume)
        current_time += beat_duration


    final_time = time.time()
    print("Time to add percussion line: " + str(final_time - initial_time) + " seconds")


def create_info_file(song, filename) :
    file = open(filename + "-info.txt", "w")
    bpm = song.get_bpm()
    duration = song.get_duration()
    seconds = int(60 * duration / bpm)
    file.write(filename + " info:\n")
    file.write("Duration: " + str(seconds) + " sec.\n")
    file.write("BPM: " + str(bpm) + "\n")
    file.write("--------------------------------------\n")
    file.write("Drum Beat: \n")
    percussion = song.get_percussion()
    for i in range (0, len(percussion)) :
        beat = percussion[i]
        name = beat.get_name()
        beat_duration = beat.get_beat_duration()
        volume = beat.get_volume()
        emotion = beat.get_emotion()
        file.write("\t"+name + ":\n")
        file.write("\t\tduration: " + str(beat_duration) + " -- volume: " + str(volume) + " -- emotion: " + str(emotion) + "\n\n")
        file.write("-------------------------------------------------------------\n")

    file.write("Chords:\n")
    chords = song.get_chords()
    for i in range (0, len(chords)) :
        chord = chords[i]
        name = chord.get_name()
        volume = chord.get_volume()
        is_major = chord.major()
        if (is_major) :
            type = "M"
        else :
            type = "m"
        file.write("\t" + name + type + ": \n")
        file.write("\t Volume: " + str(volume))
        notes = chord.get_notes()
        octave = chord.get_octave()
        for j in range (0, len(notes)) :
            note = notes[j]
            file.write("\t\tNote " + str(j) + ": " + str(note+octave) + "\n")
        file.write("------------------------------------------------------------------\n")

    notes = song.get_notes()
    for i in range (0, len(notes)) :
        note = notes[i]
        start_time = note.get_start_time()
        duration_in_beats = note.get_duration()
        pitch = note.get_pitch()
        volume = note.get_volume()
        if (pitch > 127):
            pitch = 127
        file.write("Note: \n")
        file.write("\t" + str(pitch) + " -- start: " + str(start_time) + " -- duration: " + str(duration_in_beats) + " -- volume: " + str(volume) + "\n")
        file.write("-------------------------------------------------------------------\n")

    file.close()





def random_percussion (midifile, perc, track, channel, total_duration) :
    first, second = perc.select_beats_random()
    counter = 1
    for i in range(0, total_duration, 4):
        if (counter % 4 == 0):
            current = second
        else:
            current = first
        for j in range(0, len(current)):
            beat = current[j]
            start_time = beat[0]
            duration = beat[1]
            pitch = beat[2]
            midifile.addNote(track, channel, pitch, start_time + i, duration, 100)
        counter += 1

# Channel 2
def add_guitar (midifile, track, channel, chords, bpm, total_duration) :
    program = 27
    midifile.addTempo(track, 0, bpm)
    midifile.addProgramChange(track, channel, 0, program)
    initial_time = time.time()

    number_chords = len(chords)
    for i in range(0, number_chords):
        chord = chords[i]
        start_time = i * NOTES_PER_CHORD
        while (start_time < total_duration):
            for j in range(0, NOTES_PER_CHORD):
                note = chord.get_note(j)
                midifile.addNote(track, channel, note + BASS_OCTAVE, start_time + j, 1, 100)
            start_time += number_chords * NOTES_PER_CHORD

    final_time = time.time()
    print("Time to add bass line: " + str(final_time - initial_time) + " seconds")

def generate_notes (notes, filename) :

    midifile = MidiFile.MIDIFile(removeDuplicates = False, adjust_origin = False)
    track = 0
    channel = 0
    time = 0
    duration = 1
    volume = 100

    midifile.addTempo(track, time, 120)

    for x in range (0, notes.shape[0]) :
        for y in range (0, notes.shape[1]) :
            pitch = notes[x][y]
            if (pitch == -2) :
                pass
                # Black or Gray
                # TODO
            elif (pitch == -1) :
                time +=1
            else :
                midifile.addNote(track, channel, pitch, time, duration, volume)
                time += 1

    print(time)
    print("Finished Cycle")
    with open("../results/" + filename + ".midi", 'wb') as output_file:
        midifile.writeFile(output_file)
    print("Finished writing")

def seconds_to_beats (seconds, bpm) :
    bps = int (bpm / 60)
    beats = seconds * bps
    return beats

def load_chord(note, is_major) :
    chords = numpy.genfromtxt("chords.txt", dtype='str')
    result = []
    #number = note2number(note)
    if (is_major) :
        for i in range (0, 3) :
            letter = chords[note][i]
            number = note2number(letter)
            result.append(number)
    else :
        for i in range (3, 6) :
            letter = chords[note][i]
            number = note2number(letter)
            result.append(number)

    return result

def load_guitar_chords(note, is_major) :
    chords = numpy.genfromtxt("guitar_chords.txt", dtype = 'str')
    result = []
    if (is_major) :
        line = chords[note]
    else :
        line = chords[note+12]

    for i in range (0, len(line)) :
        if (line[i] == "-1") :
            break
        result.append(int(line[i]))
    return result

def note2number(note) :
    num = -1
    if (note == 'C') :
        num = 0
    elif (note == 'Cs' or note == 'Df') :
        num = 1
    elif (note == 'D') :
        num = 2
    elif (note == 'Ds' or note == 'Ef') :
        num = 3
    elif (note == 'E'):
        num = 4
    elif (note == 'F'):
        num = 5
    elif (note == 'Fs' or note == 'Gf') :
        num = 6
    elif (note == 'G'):
        num = 7
    elif (note == 'Gs' or note == 'Af'):
        num = 8
    elif (note == 'A'):
        num = 9
    elif (note == 'As' or note == 'Bf') :
        num = 10
    elif (note == 'B') :
        num = 11
    return num

def number2note(number) :
    if (number == 0) :
        return 'C'
    elif (number == 1) :
        return 'Cs / Df'
    elif(number == 2) :
        return 'D'
    elif (number ==3) :
        return 'Ds / Ef'
    elif (number == 4) :
        return 'E'
    elif (number == 5) :
        return 'F'
    elif (number == 6) :
        return 'Fs / Gf'
    elif (number == 7) :
        return 'G'
    elif (number == 8) :
        return 'Gs / Af'
    elif (number == 9) :
        return 'A'
    elif(number == 10) :
        return 'As / Bf'
    else :
        return 'B'

def identify_octave(note) :
    if (note < 12) :
        return 0
    elif (note < 24) :
        return 12
    elif (note < 36) :
        return 24
    elif (note < 48) :
        return 36
    elif (note < 60) :
        return 48
    elif (note < 72) :
        return 60
    elif (note < 84) :
        return 72
    elif (note < 96) :
        return 84
    elif (note < 108) :
        return 96
    elif (note < 120) :
        return 108
    else :
        return 120

def identify_note(note) :
    if (note < 12) :
        return note
    elif (note < 24) :
        return note - 12
    elif (note < 36) :
        return note - 24
    elif (note < 48) :
        return note - 36
    elif (note < 60) :
        return note - 48
    elif (note < 72) :
        return note - 60
    elif (note < 84) :
        return note - 72
    elif (note < 96) :
        return note - 84
    elif (note < 108) :
        return note - 96
    elif (note < 120) :
        return note - 108
    else :
        return note - 120

def load_scale(scale, type) :
    scales = numpy.genfromtxt("scales.txt", dtype='str')
    result = []
    if(type) :
        line = scales[scale]
        for i in range (0, len(line)) :
            note = line[i]
            number = note2number(note)
            result.append(number)
    else :
        line = scales[scale+12]
        for i in range (0, len(line)) :
            note = line[i]
            number = note2number(note)
            result.append(number)
    return result






