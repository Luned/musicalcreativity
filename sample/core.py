import image_processing
import cross_domain_mapping
import quadrant_notes
import composer
import image_quadrants
import genetic_algorithm
import time
import datetime

LOAD = False


def main_function () :

    s = 'picture_files/'
    filename = input('Write the name of your file: ')
    s += filename
    s += '.jpg'

    print("Choose the point from where you want to start reading the image: ")
    print("\t Top, left corner (tl)")
    print("\t Top, right corner (tr)")
    print("\t Center (c)")
    print("\t Bottom, left corner (bl)")
    print("\t Top, right corner (br)")
    start_point = input()

    print_orientation(start_point)
    orientation = input()

    print(datetime.datetime.now().time())
    start_time = time.time()

    result = image_processing.retrieve_image_pixels(s, start_point, orientation)

    image = result[0]
    rgb = result[1]
    lab = result[2]
    rgb_pixel_count = result[3]
    lab_pixel_count = result[4]

    avg_size = image_processing.variation_histogram(lab)
    bpm = cross_domain_mapping.bpm_conversion_function(avg_size)
    warmth, avg_lightness, avg_saturation = image_processing.image_temperature(rgb_pixel_count)

    quadrants, beats = image_quadrants.calculate(filename, rgb, bpm)

    warmth, avg_saturation, avg_lightness = image_processing.image_temperature_quads(quadrants)

    avg_volume = cross_domain_mapping.saturation_conversion(avg_saturation)
    avg_octave = cross_domain_mapping.lightness_conversion(avg_lightness)
    raw_song = composer.Song(bpm, beats, avg_octave, avg_volume, 'raw')
    harmonized_song = composer.Song(bpm, beats, avg_octave, avg_volume, 'harm')
    genetic_song = composer.Song(bpm, beats, avg_octave, avg_volume, 'genetic')


    quadrant_notes.calculate(quadrants, raw_song)
    quadrant_notes.calculate(quadrants, harmonized_song)

    quadrant_notes.calculate_chords(raw_song, harmonized_song, quadrants, warmth)
    cross_domain_mapping.calculate_percussion(raw_song, harmonized_song, genetic_song, quadrants, avg_size, warmth, avg_saturation, avg_lightness)

    genetic_algorithm.calculate(raw_song, harmonized_song, genetic_song, filename)
    composer.generate_song(raw_song, filename)
    composer.generate_song(harmonized_song, filename)

    composer.generate_song(genetic_song, filename)
    final_time = time.time()
    print("Total time: " + str((final_time-start_time) / 60) + " minutes (or " + str(final_time-start_time)  +" seconds.)")

    print(datetime.datetime.now().time())
    return 0



def print_orientation(start_point) :
    print("Choose the directions in which you want the image to be read:")
    if (start_point == "tl") :
        print("\t Left to Right first, then downwards (LRD)")
        print("\t Downwards first, then left to right (DLR)")
    elif (start_point == "tr") :
        print("\t Right to Lef first, then downwards (RLD)")
        print("\t Downwards first, then right to left (DRL)")
    elif (start_point == "dl") :
        print("\t Left to Right first, then upwards (LRU)")
        print("\t Upwards first, then left to right (ULR)")
    elif (start_point == "dr") :
        print("\t Right to left first, then upwards (RLU)")
        print("\t Upwards first, then right to left (URL)")
    elif (start_point == "c") :
        print("\t Outwards, in a spiral motion (O)")
        print("\t Left to right, then downwards (LRD)")
        print("\t Left to right, then upwards (LRU)")
        print("\t Right to left, then downwards (RLD)")
        print("\t Right to left, then upwards (RLU)")

if(__name__ == '__main__') :
    main_function()
