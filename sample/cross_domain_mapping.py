import color_conversions
import cross_domain_mapping
import composer
import time
import sys

WHITE = 90
BLACK = 10
GRAY = 10
NOCTAVES = 8
INITIAL_OCTAVE = 24
OCTAVE_SIZE = 12
MIN_BEATS = 1
MAX_BEATS = 4
MIN_SAT = 25

MIN_BPM = 50
MAX_BPM = 150

MIN_TIME = 60
MAX_TIME = 180

BPM_DIF = 15
DURATION = False

VAR_WEIGHT = 0.25
WARM_WEIGHT = 0.25
SAT_WEIGHT = 0.25
LIGHT_WEIGHT = 0.25

def hue_conversion_function (hue) :
    #Red
    if (hue <= 10) :
        note = 0
    #Red-Orange
    elif (hue <= 20) :
        note = 1
    #Orange
    elif (hue <= 30) :
        note = 2
    #Yellow-Orange
    elif (hue <= 50):
        note = 3
    #Yellow
    elif (hue <= 70):
        note = 4
    #Yellow-Green
    elif (hue <= 100):
        note = 5
    #Green
    elif (hue <= 140):
        note = 6
    #Blue-Green
    elif (hue <= 170):
        note = 7
    #Blue
    elif (hue <= 230):
        note = 8
    #Blue-Purple
    elif (hue <= 280):
        note = 9
    #Purple
    elif (hue <= 320):
        note = 10
    #Red-Purple
    elif (hue <= 350):
        note = 11
    else :
        note = 0
    return note

def saturation_conversion (saturation) :
    if (saturation < MIN_SAT) :
        vol = int(1.27 * MIN_SAT)
    else :
        vol = int(1.27 * saturation)
    return vol

def lightness_conversion (lightness) :
    div = 100 / NOCTAVES
    if (lightness <= div) :
        octave = INITIAL_OCTAVE
    elif (lightness <= 2*div) :
        octave = INITIAL_OCTAVE + OCTAVE_SIZE
    elif (lightness <= 3*div) :
        octave = INITIAL_OCTAVE + (2 * OCTAVE_SIZE)
    elif (lightness <= 4*div) :
        octave = INITIAL_OCTAVE + (3 * OCTAVE_SIZE)
    elif (lightness <= 5*div) :
        octave = INITIAL_OCTAVE + (4 * OCTAVE_SIZE)
    elif (lightness <= 6*div) :
        octave = INITIAL_OCTAVE + (5 * OCTAVE_SIZE)
    elif (lightness <= 7*div) :
        octave = INITIAL_OCTAVE + (6 * OCTAVE_SIZE)
    else :
        octave = INITIAL_OCTAVE + (7 * OCTAVE_SIZE)

    return octave

def calculate_percussion(raw_song, harmonized_song, quadrants, avg_region_size, warmness, avg_saturation,
                         avg_lightness):

    percussion = composer.Percussion()
    beats = percussion.get_beats()
    calculate_percussion_raw(beats, quadrants, raw_song)
    calculate_percussion_harmonized(avg_lightness, avg_region_size, avg_saturation, warmness, harmonized_song)

def calculate_percussion_harmonized(avg_lightness, avg_region_size, avg_saturation, warmness, harmonized_song):
    image_emotion = (VAR_WEIGHT * (100 - avg_region_size)) + (WARM_WEIGHT * warmness) + (SAT_WEIGHT * avg_saturation) + (LIGHT_WEIGHT * avg_lightness)
    percussion = composer.Percussion()
    beats = percussion.get_beats()
    solution = None
    lowest_dif = sys.maxsize
    second_lowest = sys.maxsize
    second = None
    for i in range(0, len(beats)):
        beat = beats[i]
        beat_emotion = beat.get_emotion()
        dif = abs(beat_emotion - image_emotion)
        if (dif < lowest_dif):
            if (lowest_dif < second_lowest):
                second_lowest = lowest_dif
                second = solution
            lowest_dif = dif
            solution = beat
        else:
            if (dif < second_lowest):
                second_lowest = dif
                second = beat

    harmonized_beat_distribution(harmonized_song, second, solution)

def harmonized_beat_distribution(harmonized_song, second, solution):
    volume = harmonized_song.get_avg_volume()
    total_duration = harmonized_song.get_duration()
    counter = 0
    current_time = 0
    while (current_time < total_duration):
        if (counter >= 16):
            beat = second.copy_beat()
            counter = 0
        else:
            beat = solution.copy_beat()
            counter += beat.get_beat_duration()

        beat.set_volume(volume)
        beat_duration = beat.get_beat_duration()
        current_time += beat_duration
        harmonized_song.add_percussion(beat)

def calculate_percussion_raw(beats, quadrants, raw_song):
    solution = None
    lowest_dif = sys.maxsize
    for i in range(0, len(quadrants)):
        quad = quadrants[i]
        avg_quad_region_size = quad.get_avg_region_size()
        warmth = quad.get_warmth()
        sat = quad.get_saturation()
        light = quad.get_lightness()
        quad_emotional_value = WARM_WEIGHT * warmth + VAR_WEIGHT * (100 - avg_quad_region_size) + SAT_WEIGHT * sat + LIGHT_WEIGHT * light
        for j in range(0, len(beats)):
            beat = beats[j]
            beat_emotion = beat.get_emotion()
            dif = abs(beat_emotion - quad_emotional_value)
            if (dif < lowest_dif):
                lowest_dif = dif
                solution = beat
        new = solution.copy_beat()
        new.set_volume(cross_domain_mapping.saturation_conversion(sat))
        raw_song.add_percussion(new)

def bpm_conversion_function (avg_size) :
    if (avg_size > 55) :
        return 50
    else :
        return int (((MIN_BPM - MAX_BPM) / 55) * avg_size + MAX_BPM)

def duration_interval (bpm) :
    min_beats = int((MIN_TIME * bpm)/60)
    max_beats = int((MAX_TIME * bpm)/60)
    print("BEAT INTERVAL: " + str(min_beats) + ", " + str(max_beats))
    return (min_beats, max_beats)