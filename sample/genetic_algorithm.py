import random
import composer
import time
import sys
import datetime
import numpy as np
import object_serialization as objs


MAX_POP = 24
CROSSOVER = 90
NOTE_MUTATION = 15
BAR_MUTATION = 10
BEAT_MUTATION = 5
ELITISM = 0.25

BAR_CROSSOVER = 40
BAR_NOTE_CROSSOVER = 80
CHORD_CROSSOVER = 90
NOTE_CROSSOVER = 100

PITCH_MUTATION = 50
OCTAVE_MUTATION = 75
TIME_MUTATION = 100
MAX_BAR_NOTES = 16

NUMBER_ITERATIONS = 300


class GeneticNote:

    def __init__(self, pitch, start, duration, volume):
        self.pitch = pitch
        self.start = start
        self.duration = duration
        self.volume = volume

    def get_pitch(self):
        return self.pitch

    def get_start(self):
        return self.start

    def get_duration(self):
        return self.duration

    def get_volume(self):
        return self.volume

    def set_pitch(self, pitch):
        self.pitch = pitch

    def set_start(self, start):
        self.start = start

    def crossover(self, note):
        first_pitch = self.pitch
        sec_pitch = note.get_pitch()
        self.pitch = sec_pitch
        note.set_pitch(first_pitch)

    def mutation(self, current_index, bar, file):
        mutation_done = False
        mut = random.randint(0, 99)
        if (mut < NOTE_MUTATION):
            if (current_index == bar.get_num_notes() - 1):
                mutation_type = random.randint(0, 74)
            else:
                mutation_type = random.randint(0, 99)

            if (mutation_type < PITCH_MUTATION):
                #file.write("Pitch Mutation\n")
                new_pitch = random.randint(0, 12)
                octave = composer.identify_octave(self.pitch)
                if (new_pitch == 12):
                    self.pitch = -1
                else:
                    self.pitch = new_pitch + octave
                mutation_done = True

            elif (mutation_type < OCTAVE_MUTATION):
                #file.write("Octave Mutation\n")
                octaves = [24, 36, 48, 60, 72, 84, 96, 108]
                index = random.randint(0, len(octaves)-1)
                new_octave = octaves[index]
                note = composer.identify_note(self.pitch)
                self.pitch = note + new_octave
                mutation_done = True
            elif (mutation_type < TIME_MUTATION):
                notes = bar.get_notes()
                next_note = notes[current_index+1]
                self.duration += next_note.get_duration()
                for i in range(current_index+2, bar.get_num_notes()):
                    previous = notes[i-1]
                    current = notes[i]
                    previous.new_obj(current)
                bar.reduce_num_notes()
                mutation_done = True
        return mutation_done

    def equal(self, note):
        return self.pitch == note.get_pitch()

    def interval(self, note):
        if (self.pitch == -1 or note.get_pitch() == -1) :
            return 0
        return abs(self.pitch - note.get_pitch())

    def composer_note(self):
        note = composer.Note(self.pitch, self.start, self.duration, self.volume)
        return note

    def copy_note(self):
        new_note = GeneticNote(self.pitch, self.start, self.duration, self.volume)
        return new_note

    def new_obj(self, note):
        self.pitch = note.get_pitch()
        self.start = note.get_start()
        self.duration = note.get_duration()
        self.volume = note.get_volume()

    def print_note(self):
        print("\t\t\t\tPitch: " + str(self.pitch))
        print("\t\t\t\tStart: " + str(self.start))
        print("\t\t\t\tDuration: " + str(self.duration))

    def write_note(self, file):
        file.write("\t\t\t\tPitch: " + str(self.pitch) + "\n")
        file.write("\t\t\t\tStart: " + str(self.start) + "\n")
        file.write("\t\t\t\tDuration: " + str(self.duration) + "\n")
        file.write("\t\t\t\tVolume: " + str(self.volume) + "\n")


class Bar:

    def __init__(self, notes, chord, beat, num_notes):
        self.notes = notes
        self.chord = chord.copy_chord()
        self.beat = beat.copy_beat()
        self.num_notes = num_notes

    def get_notes(self):
        return self.notes

    def get_note(self, i):
        if i < 0 or i >= self.num_notes:
            print("Error: i should be positive and in range of the array - " + str(i))
            sys.exit()
        else:
            return self.notes[i]

    def get_chord(self):
        return self.chord

    def get_beat(self):
        return self.beat

    def get_num_notes(self):
        return self.num_notes

    def set_chord(self, chord):
        self.chord = chord

    def reduce_num_notes(self):
        self.num_notes -= 1

    def bar_size(self):
        return self.num_notes

    def set_notes(self, notes):
        self.notes = notes

    def set_beat(self, beat):
        self.beat = beat

    def copy_bar(self):
        new_notes = np.empty(self.notes.size, dtype=GeneticNote)
        for i in range(0, self.num_notes):
            note = self.notes[i]
            new = note.copy_note()
            new_notes[i] = new
        for i in range(self.num_notes, self.notes.size):
            new_notes[i] = GeneticNote(0, 0, 0, 0)
        new_chord = self.chord.copy_chord()
        new_beat = self.beat.copy_beat()

        return Bar(new_notes, new_chord, new_beat, self.num_notes)

    def crossover(self, bar):
        first_size = self.bar_size()
        sec_size = bar.bar_size()
        if (sec_size == 0 or first_size == 0):
            pass
        elif (sec_size < 2 or first_size < 2):
            first_note = self.notes[0]
            second_note = bar.get_note(0)
            first_note.crossover(second_note)
        else:
            crossover_point = random.randint(1, sec_size-1)
            if(first_size != sec_size):
                start_point = random.randint(0, first_size - sec_size)
            else:
                start_point = 0
            j = 0
            for i in range(start_point, start_point + sec_size):
                first_note = self.get_note(i)
                sec_note = bar.get_note(j)

                if (j >= crossover_point):
                    first_note.crossover(sec_note)
                j += 1

    def chord_crossover(self, bar):
        first_chord = self.chord
        sec_chord = bar.get_chord()
        self.chord = sec_chord
        bar.set_chord(first_chord)

    def note_crossover(self, bar):
        first_notes = self.notes
        sec_notes = bar.get_notes()
        self.notes = sec_notes
        bar.set_notes(first_notes)

    def mutation(self, file):
        for i in range(0, self.num_notes):
            note = self.notes[i]
            mut_done = note.mutation(i, self, file)
            if(mut_done):
                break

        mut = random.randint(0, 99)
        if (mut < BAR_MUTATION):
            self.chord_mutation()
            #file.write("Chord Mutation\n")

        mut = random.randint(0, 99)
        if (mut < BEAT_MUTATION):
            self.beat_mutation()
            #file.write("Beat Mutation\n")

    def chord_mutation(self):
        new_chord = random.randint(0, 11)
        major = random.randint(0, 1)
        if(major == 0):
            chord_type = "minor"
        else:
            chord_type = "major"

        volume = self.chord.get_volume()
        octave = self.chord.get_octave()
        self.chord = composer.Chord(new_chord, chord_type, -1, octave, volume)

    def beat_mutation(self):
        percussion = composer.Percussion()
        new_beat = percussion.get_random_beat()
        name = new_beat.get_name()
        bpm = self.beat.get_bpm()
        volume = self.beat.get_volume()
        beat_duration = new_beat.get_beat_duration()
        beats = new_beat.get_beats()
        total_notes = new_beat.get_total_notes()
        sum_durations = new_beat.get_sum_durations()

        self.beat = composer.Beat(name, bpm, volume, beat_duration, beats, total_notes, sum_durations)

    def calculate_fitness(self):
        fitness = 0
        total = 0
        total_downbeat_fitness = 0
        for i in range(0, self.num_notes):
            note_repetition = self.note_repetition_fitness(i)
            interval_fitness = self.interval_fitness(i)
            downbeat_fitness, num = self.downbeat_fitness(i)
            total += num
            total_downbeat_fitness += downbeat_fitness
            long_note_fitness = self.long_note_fitness(i)
            #short_note_fitness = self.short_note_fitness(i)
            fitness += note_repetition + interval_fitness + long_note_fitness
        if (total > 0):
            total_downbeat_fitness = int(total_downbeat_fitness/total)
        return fitness + total_downbeat_fitness

    def note_repetition_fitness(self, i):
        fitness = 0
        if(self.num_notes < 2 or i < 1):
            return fitness
        else:
            previous = self.notes[i-1]
            current = self.notes[i]
            if (current.equal(previous)):
                fitness -= 10
        return fitness

    def interval_fitness(self, i):
        fitness = 0
        if (i < 1):
            return fitness
        else:
            previous = self.notes[i-1]
            current = self.notes[i]
            interval = previous.interval(current)
            if (interval > 12):
                fitness -= 20
        return fitness

    def downbeat_fitness(self, i):
        bar_start = self.notes[0].get_start()
        fitness = 0
        chord_notes = self.chord.get_notes()
        chord = self.chord.get_chord()
        chord_type = self.chord.major()
        scale = composer.load_scale(chord, chord_type)
        num = 0
        note = self.notes[i]
        pitch = composer.identify_note(note.get_pitch())
        # We are only interested in the first beat
        if (note.get_start() >= bar_start + 1):
            return (fitness, num)
        num += 1
        if (pitch in chord_notes):
            fitness += 15
        elif (note.get_pitch() == -1):
            fitness += 10
        elif (pitch in scale):
            fitness += 5
        else:
            fitness -= 10

        return (fitness, num)

    def long_note_fitness(self, i):
        fitness = 0
        chord_notes = self.chord.get_notes()
        chord = self.chord.get_chord()
        chord_type = self.chord.major()
        scale = composer.load_scale(chord, chord_type)

        note = self.notes[i]
        pitch = composer.identify_note(note.get_pitch())
        if (note.get_duration() >= 2):
            if(note.get_pitch() == -1):
                fitness -= 15
            elif (pitch in chord_notes):
                fitness += 15
            elif (pitch in scale):
                fitness += 5
            else:
                fitness -= 10
        return fitness

    def short_note_fitness(self, i):
        fitness = 0
        note = self.notes[i]
        duration = note.get_duration()
        if (duration < 0.25):
            fitness -= 15
        elif (duration <= 2):
            fitness += 15

        return fitness

    def randomize_bar(self, start_time):
        placeholder_notes = []
        for i in range(0, self.num_notes):
            placeholder_notes.append(self.notes[i])
        random.shuffle(placeholder_notes)

        for i in range(0, self.num_notes):
            note = placeholder_notes[i]
            note.set_start(start_time)
            self.notes[i] = placeholder_notes[i]
            start_time += note.get_duration()

        for i in range(self.num_notes, self.notes.size):
            self.notes[i] = GeneticNote(0, 0, 0, 0)

    def new_obj(self, bar):
        chord = bar.get_chord()
        beat = bar.get_beat()

        self.num_notes = bar.get_num_notes()
        self.chord.copy_into_chord(chord)
        self.beat.copy_into_beat(beat)
        for i in range(0, bar.get_num_notes()):
            note = self.notes[i]
            other = bar.get_note(i)
            note.new_obj(other)

    def print_bar(self):
        chord_type = self.chord.major()
        if (chord_type):
            chord_string = "M"
        else:
            chord_string = "m"
        print("\t\t\tChord: " + self.chord.get_name() + chord_string)
        print("\t\t\tBeat: " + str(self.beat.get_name()))
        print("\t\t\tNotes: ")
        for i in range(0, self.num_notes):
            note = self.notes[i]
            note.print_note()

    def write_bar(self, file):
        chord_type = self.chord.major()
        if (chord_type):
            chord_string = "M"
        else:
            chord_string = "m"
        file.write("\t\t\tChord: " + self.chord.get_name() + chord_string + "\n")
        file.write("\t\t\tBeat: " + self.beat.get_name() + "\n")
        file.write("\t\t\tNotes: \n")
        for i in range(0, self.num_notes):
            note = self.notes[i]
            note.write_note(file)


class Individual:

    def __init__(self, bars, fitness=-1):
        self.bars = bars
        self.fitness = fitness
        self.chord_order = []

    def __lt__(self, other):
        return self.fitness > other.get_fitness()

    def generate_order(self):
        self.chord_order = []
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            chord = bar.get_chord()
            self.chord_order.append(chord)

    def notes2genetic_notes(self, notes, chords, beats, avg_volume):
        bars = []
        bar_notes = []
        bar_end_time = 4
        previous_end = 0
        note_index = 0
        sorted_notes = sorted(notes, key=lambda x: x.start_time)
        for i in range(0, len(chords)):
            bar_finished = False
            while(not bar_finished):
                if (len(sorted_notes) == 0) :
                    rest = GeneticNote(-1, previous_end, 4, avg_volume)
                    previous_end = bar_end_time
                    bar_notes.append(rest)
                if(note_index >= len(sorted_notes)):
                    bar_end_time, bar_notes = self.create_placeholder_bar(bars, bar_end_time,
                                                                          bar_notes, chords[i], beats[i])
                    previous_end = bar_end_time
                    break
                note = sorted_notes[note_index]
                note_start = note.get_start_time()
                if (previous_end < note_start):
                    if (note_start >= bar_end_time):
                        rest = GeneticNote(-1, previous_end, bar_end_time-previous_end, avg_volume)
                        previous_end = bar_end_time
                        bar_notes.append(rest)
                        bar_end_time, bar_notes = self.create_placeholder_bar(bars, bar_end_time,
                                                                              bar_notes, chords[i], beats[i])
                        bar_finished = True
                    else:
                        rest = GeneticNote(-1, note_start, note_start-previous_end, avg_volume)
                        previous_end = note_start
                        bar_notes.append(rest)
                        continue
                else:
                    if (note_start >= bar_end_time):
                        bar_end_time, bar_notes = self.create_placeholder_bar(bars, bar_end_time,
                                                                              bar_notes, chords[i], beats[i])
                        previous_end = bar_end_time
                        bar_finished = True
                    else:
                        self.create_genetic_note(bar_notes, note)
                        note_index += 1
                        previous_end = note_start + note.get_duration()
        return bars

    def create_placeholder_bar(self, bars, bar_end_time, bar_notes, chord, beat):
        bar = (bar_notes, chord, beat)
        bars.append(bar)
        bar_end_time += 4
        bar_notes = []
        return bar_end_time, bar_notes

    def create_genetic_note(self, bar_notes, note):
        start_time = note.get_start_time()
        # Test if there was a silence between the previous note and the current
        pitch = note.get_pitch()
        duration = note.get_duration()
        volume = note.get_volume()
        genetic_note = GeneticNote(pitch, start_time, duration, volume)
        bar_notes.append(genetic_note)

    def generate_bar_objects(self, bars):
        for i in range(0, len(bars)):
            bar = bars[i]
            notes = bar[0]
            chord = bar[1]
            beat = bar[2]
            new_notes = np.empty(MAX_BAR_NOTES, dtype=GeneticNote)
            for j in range(0, new_notes.size):
                if (j >= len(notes)):
                    new_notes[j] = GeneticNote(0, 0, 0, 0)
                else:
                    new_notes[j] = notes[j]

            self.bars[i] = Bar(new_notes, chord, beat, len(notes))
        self.generate_order()
        self.calculate_fitness()

    def copy_individual(self):
        new_bars = np.empty(self.bars.size, dtype=Bar)
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            new = bar.copy_bar()
            new_bars[i] = new
        fitness = self.fitness
        new_ind = Individual(new_bars, fitness)
        new_ind.generate_order()
        return new_ind

    def randomize_individual(self):
        bars = self.get_bars()
        random.shuffle(self.chord_order)
        np.random.shuffle(bars)
        start = 0
        for i in range(0, len(bars)):
            bar = bars[i]
            bar.randomize_bar(start)
            chord = self.chord_order[i]
            bar.set_chord(chord)
            start += 4

    def create_random_individual(self):
        new_individual = self.copy_individual()
        new_individual.randomize_individual()
        new_individual.calculate_fitness()
        return new_individual

    def calculate_fitness(self):
        fitness = 0
        lowest = 127
        highest = 0
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            progression = self.progression_fitness(i)
            chord_repetition = self.chord_repetition_fitness(i)
            low, high = self.range_fitness(bar, lowest, highest)
            if (low < lowest and low != -1):
                lowest = low
            if (high > highest):
                highest = high
            bar_fitness = bar.calculate_fitness()
            fitness += progression + chord_repetition + bar_fitness


        if (highest - lowest < 18):
            fitness += 10
        self.fitness = fitness

    def progression_fitness(self, i):
        fitness = 0
        if(i < 2):
            return fitness
        else:
            progressions = composer.RockProgressions()
            chord = self.bars[i-2].get_chord()
            first = (chord.get_note(0), chord.major())
            chord = self.bars[i-1].get_chord()
            second = (chord.get_note(0), chord.major())
            chord = self.bars[i].get_chord()
            current = (chord.get_note(0), chord.major())
            prog_solution = progressions.is_prog([first, second, current])
            fitness += self.progression_cases(prog_solution[0], prog_solution[1])

        return fitness

    def progression_cases(self, best_prog, rock_chords):
        # The 3 chords make a rock progression
        if (best_prog == 3):
            return 25
        # Only 2 chords make a rock progression
        elif (best_prog == 2):
            # The non progression chord is a rock chord
            if (rock_chords == 3):
                return 15
            # The non progression chord is not a rock chord
            else:
                return 10
        #There is no rock progression
        else:
            # All chords are rock chords
            if(rock_chords == 3):
                return 5
            # 2 chords are rock chords
            elif (rock_chords == 2):
                return 0
            # Only 1 chord is rock
            elif (rock_chords == 1):
                return -10
            else:
                return -25

    def chord_repetition_fitness(self, i):
        fitness = 0
        if (i < 2):
            return fitness
        else:
            first = self.bars[i-2].get_chord()
            second = self.bars[i-1].get_chord()
            current = self.bars[i].get_chord()
            if (current.get_name() == second.get_name() and current.get_name() == first.get_name()):
                fitness -= 25
        return fitness

    def range_fitness(self, bar, lowest, highest):
        notes = bar.get_notes()
        for i in range(0, bar.get_num_notes()):
            note = notes[i]
            pitch = note.get_pitch()
            if (pitch < lowest):
                lowest = pitch
            if (pitch > highest):
                highest = pitch
        return (lowest, highest)

    def get_bars(self):
        return self.bars

    def get_bar(self, i):
        if (i < 0 or i >= self.bars.size):
            print("Error: i should be positive and in range of the array")
            sys.exit()
        else:
            return self.bars[i]

    def get_fitness(self):
        return self.fitness

    def crossover(self, individual, file):
        rand_num = random.randint(0, 99)
        if (rand_num < BAR_CROSSOVER):
            self.bar_crossover(individual)
            #file.write("Bar Crossover\n")
        elif (rand_num < BAR_NOTE_CROSSOVER):
            self.bar_note_crossover(individual)
            #file.write("Bar Note Crossover\n")
        elif (rand_num < CHORD_CROSSOVER):
            self.chord_crossover(individual)
            #file.write("Chord Crossover\n")
        else:
            self.note_crossover(individual)
            #file.write("Note Crossover\n")

    def bar_crossover(self, individual):
        cross_point = random.randint(1, self.bars.size-1)
        ind_bars = individual.get_bars()
        for i in range(0, self.bars.size):
            first_bar = self.bars[i]
            sec_bar = ind_bars[i]
            if (i >= cross_point):
                self.bars[i] = sec_bar
                ind_bars[i] = first_bar
        self.generate_order()
        individual.generate_order()

    def bar_note_crossover(self, individual):
        chosen_bar = random.randint(0, self.bars.size-1)
        first_bar = self.bars[chosen_bar]
        sec_bar = individual.get_bar(chosen_bar)
        if (first_bar.bar_size() > sec_bar.bar_size()):
            first_bar.crossover(sec_bar)
        else:
            sec_bar.crossover(first_bar)

    def chord_crossover(self, individual):
        cross_point = random.randint(1, self.bars.size-1)
        ind_bars = individual.get_bars()
        for i in range(0, self.bars.size):
            first_bar = self.bars[i]
            sec_bar = ind_bars[i]
            if (i >= cross_point):
                first_bar.chord_crossover(sec_bar)
        self.generate_order()
        individual.generate_order()

    def note_crossover(self, individual):
        cross_point = random.randint(1, self.bars.size-1)
        ind_bars = individual.get_bars()
        for i in range(0, self.bars.size):
            first_bar = self.bars[i]
            sec_bar = ind_bars[i]
            if (i >= cross_point):
                first_bar.note_crossover(sec_bar)
        self.generate_order()
        individual.generate_order()

    def mutation(self, individual, file):
        # file.write("Mutation: \n")
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            other = individual.get_bar(i)
            bar.mutation(file)
            other.mutation(file)

    def new_obj(self, individual):
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            other = individual.get_bar(i)
            bar.new_obj(other)
        self.fitness = individual.get_fitness()

    def create_song(self, song):
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            chord = bar.get_chord()
            beat = bar.get_beat()
            song.add_chord(chord)
            song.add_percussion(beat)
            notes = bar.get_notes()
            for j in range(0, bar.get_num_notes()):
                note = notes[j]
                pitch = note.get_pitch()
                if (pitch == -1):
                    continue
                song.add_note(note.composer_note())

    def print_individual(self):
        print("\tFitness: " + str(self.fitness))
        print("\tBars: " + str(self.bars.size))
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            print("\t\tBar " + str(i))
            bar.print_bar()
            print("\t\t################")

    def write_individual(self, file):
        file.write("\tFitness: " + str(self.fitness) + "\n")
        file.write("\tBars: " + str(self.bars.size) + "\n")
        for i in range(0, self.bars.size):
            bar = self.bars[i]
            file.write("\t\tBar " + str(i) + "\n")
            bar.write_bar(file)
            file.write("\t\t############\n")


class Population:

    def __init__(self):
        self.individuals = np.empty(MAX_POP, dtype=Individual)
        self.back_up_objects = np.empty(MAX_POP, dtype=Individual)
        self.fitness_sum = 0

    def generate_initial_population(self, raw_song, harmonized_song):
        raw_notes = raw_song.get_notes()
        harmonized_notes = harmonized_song.get_notes()
        raw_chords = raw_song.get_chords()
        harmonized_chords = harmonized_song.get_chords()
        raw_beats = raw_song.get_percussion()
        harm_beats = harmonized_song.get_percussion()

        raw = Individual(np.empty(len(raw_chords), dtype=Bar))
        raw_bars = raw.notes2genetic_notes(raw_notes, raw_chords, raw_beats, raw_song.get_avg_volume())
        harmonized = Individual(np.empty(len(harmonized_chords), dtype=Bar))
        harm_bars = harmonized.notes2genetic_notes(harmonized_notes, harmonized_chords, harm_beats,
                                                             harmonized_song.get_avg_volume())

        raw.generate_bar_objects(raw_bars)
        harmonized.generate_bar_objects(harm_bars)

        self.individuals[0] = raw
        self.individuals[1] = harmonized
        self.fitness_sum += raw.get_fitness()
        self.fitness_sum += harmonized.get_fitness()

        self.back_up_objects[0] = raw.copy_individual()
        self.back_up_objects[1] = harmonized.copy_individual()

        for i in range(0, MAX_POP-2):
            # Randomize notes and chords from raw + harmonized versions
            choice = random.randint(0, 1)
            # Generate an individual from the raw version
            if (choice == 0):
                new_individual = raw.create_random_individual()
            # Generate an individual from the harmonized version
            else:
                new_individual = harmonized.create_random_individual()
            new_individual.generate_order()
            self.individuals[i+2] = new_individual
            self.back_up_objects[i+2] = new_individual.copy_individual()
            self.fitness_sum += new_individual.get_fitness()
        print("POP SIZE: " + str(self.individuals.size))
        print("FIT SUM: " + str(self.fitness_sum))

    def new_generation(self, filename):
        file = ""
        #file = open("genetic_info/" + filename + str(number) + "-genetic-info.txt", "w")
        #file.write(filename + " - Iteration " + str(number) + " info:\n\n")
        elitism_size = int(ELITISM * MAX_POP)
        new_sum = 0
        new_pop = np.empty(MAX_POP, dtype=Individual)
        sorted_pop = np.sort(self.individuals)
        #initial_time = time.time()

        for i in range(0, elitism_size):
            individual = sorted_pop[i]
            new_ind = self.back_up_objects[i]
            new_ind.new_obj(individual)
            new_pop[i] = new_ind
            new_sum += new_ind.get_fitness()

        #print("Time for elitism: " + str(time.time() - initial_time))
        i = elitism_size
        while(True):
            #initial_time = time.time()
            first, second = self.selection(i)
            #print("Time for selection: " + str(time.time() - initial_time))
            #file.write("Selection: \n")
            # first.write_individual(file)
            # second.write_individual(file)
            # file.write("------------------------------\n")
            #initial_time = time.time()
            self.crossover(first, second, file)
            #print("Time to crossover: " + str(time.time() - initial_time))
            #initial_time = time.time()
            first.mutation(second, file)
            #second.mutation(file)
            #print("Time to Mutate: " + str(time.time() - initial_time))
            #file.write("---------------------------------\n")

            #initial_time = time.time()
            first.calculate_fitness()
            second.calculate_fitness()
            #print("Time to calculate fitness: " + str(time.time() - initial_time))

            new_pop[i] = first
            new_sum += first.get_fitness()
            i += 1
            if (i >= MAX_POP):
                break

            new_pop[i] = second
            new_sum += second.get_fitness()
            i += 1

            if (i >= MAX_POP):
                break

        self.back_up_objects = self.individuals
        self.individuals = new_pop
        self.fitness_sum = new_sum
        print("POP SIZE: " + str(self.individuals.size))
        print("FIT SUM: " + str(self.fitness_sum))

        # file.write("\n\n\n End of Iteration\n\n\n")
        # self.write_genetic_info(file)
        # file.close()

    def selection(self, current_index):
        sorted_pop = np.sort(self.individuals)
        lowest = sorted_pop[MAX_POP-1]
        correct_value = abs(lowest.get_fitness())
        corrected_sum = self.fitness_sum + (correct_value * MAX_POP)
        rand_num_first = random.randint(0, corrected_sum)
        first_found = False
        rand_num_second = random.randint(0, corrected_sum)
        second_found = False
        current_sum = 0
        first_ind = None
        sec_ind = None
        for i in range(MAX_POP-1, -1, -1):
            ind = self.individuals[i]
            current_sum += (ind.get_fitness() + correct_value)
            if(current_index >= MAX_POP):
                break
            if (current_sum >= rand_num_first and not first_found):
                first_ind = self.back_up_objects[current_index]
                first_ind.new_obj(ind)
                current_index += 1
                first_found = True
            if (current_sum >= rand_num_second and not second_found):
                sec_ind = self.back_up_objects[current_index]
                sec_ind.new_obj(ind)
                current_index += 1
                second_found = True

        return (first_ind, sec_ind)

    def crossover(self, first_ind, sec_ind, file):
        rand_num = random.randint(0, 99)
        if (rand_num < CROSSOVER):
            first_ind.crossover(sec_ind, file)

    def calculate_fitness_sum(self):
        for i in range(0, self.individuals.size):
            ind = self.individuals[i]
            self.fitness_sum += ind.get_fitness()

    def generate_solution(self, song):
        sorted_pop = np.sort(self.individuals)
        highest = sorted_pop[0]
        print(highest.get_fitness())
        highest.create_song(song)

    def max_fitness(self):
        sorted_pop = np.sort(self.individuals)
        highest = sorted_pop[0]
        return highest.get_fitness()

    def print_population(self):
        for i in range(0, self.individuals.size):
            print("-----------------------------------------------------------------------------------------")
            print("Individual " + str(i) + ":")
            ind = self.individuals[i]
            ind.print_individual()

    def write_genetic_info(self, filename):
        file = open("genetic_info/" + filename + "-genetic-info.txt", "w")
        file.write(filename + " info:\n")
        for i in range(0, self.individuals.size):
            file.write("-----------------------------------------------------\n")
            file.write("Individual " + str(i) + ":\n")
            ind = self.individuals[i]
            ind.write_individual(file)

        file.close()


def calculate(raw_song, harmonized_song, genetic_song, filename):
    pop = Population()
    pop.generate_initial_population(raw_song, harmonized_song)
    #pop.print_population()
    pop.write_genetic_info(filename)
    num_bars = len(raw_song.get_chords())
    min_fitness = (10 * num_bars) + 20 + (10 * num_bars) + (5 * 4 * num_bars)
    print("MIN: " + str(min_fitness))
    max_fit = pop.max_fitness()
    print("Highest: " + str(max_fit))
    for i in range(0, NUMBER_ITERATIONS):
        print(i)
        start_time = time.time()
        print(datetime.datetime.now().time())
        pop.new_generation(filename)
        finish_time = time.time()
        print("time: " + str(finish_time-start_time))
        print(datetime.datetime.now().time())
        max_fit = pop.max_fitness()
        print("Highest: " + str(max_fit))
        if (max_fit >= min_fitness):
            break
    pop.generate_solution(genetic_song)
    objs.dump_to_file(pop, filename+"-gen")