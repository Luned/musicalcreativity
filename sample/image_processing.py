import sys
import color_conversions
import time
import random

from PIL import Image

from skimage import io
from skimage import color

import sys
import numpy
import cv2
from multiprocessing.dummy import Pool as ThreadPool
from matplotlib import pyplot as plt

#MACROS
PREDOMINANT_COLORS = 3
C =  3
pixels_x = 150
pixels_y = 500

MIN_SHORT = 30
MAX_SHORT = 300
MIN_MED = 302
MAX_MED = 900
MIN_LONG = 902
MAX_LONG = 1800
KNOWN_RATIOS = [(1, 5), (1, 4), (1, 3), (1, 2), (2, 3), (3, 4), (5, 6), (1, 1)]

MAX_IMG = 10000000
MIN_IMG = 100

def retrieve_image_pixels (name, start_point, orientation) :
    image = cv2.imread(name, 0)
    height = image.shape[0]
    width = image.shape[1]
    # rgb_pixels = list(image.getdata())
    # rgb_pixels = numpy.array(rgb_pixels).reshape((width, height, 3))
    image_info(width, height)
    if (width * height < MIN_IMG or width * height > MAX_IMG or is_prime(height) or is_prime(width)) :
        print("Error: Your image dimensions should be bigger than " + str(MIN_IMG) + " pixels and smaller than " + str(MAX_IMG) + " pixels.")
        sys.exit()
    rgb_pixels = io.imread(name)
    lab_pixels = color.rgb2lab(rgb_pixels)
    # = Image.open(name)
    # = im.getcolors(height *
    count = color_count(rgb_pixels, lab_pixels)
    rgb_pixels, lab_pixels = reorient_pixels(rgb_pixels, lab_pixels, start_point, orientation)
    return image, rgb_pixels, lab_pixels, count[0], count[1]

def reorient_pixels (rgb_pixels, lab_pixels, start_point, orientation) :
    if(start_point == 'tl' or start_point == 'TL') :
        #normal reading orientation
        if (orientation == 'LRD' or orientation == 'lrd') :
            pass
        # Down, then right
        elif (orientation == 'DLR'or orientation == 'dlr') :
            rgb_pixels = numpy.transpose(rgb_pixels, axes = (1, 0, 2))
            lab_pixels = numpy.transpose(lab_pixels, axes = (1, 0, 2))
    elif(start_point == 'tr' or start_point == 'TR') :
        if (orientation == 'RLD'or orientation == 'rld') :
            rgb_pixels = numpy.fliplr(rgb_pixels)
            lab_pixels = numpy.fliplr(lab_pixels)
        elif(orientation == 'DRL'or orientation == 'drl') :
            rgb_pixels = numpy.transpose(numpy.fliplr(rgb_pixels), axes = (1, 0, 2))
            lab_pixels = numpy.transpose(numpy.fliplr(lab_pixels), axes = (1, 0, 2))
    elif(start_point == 'dl' or start_point == 'DL') :
        if (orientation == 'LRU'or orientation == 'lru') :
            rgb_pixels = numpy.flipud(rgb_pixels)
            lab_pixels = numpy.flipud(lab_pixels)
        elif(orientation == 'ULR'or orientation == 'ulr') :
            rgb_pixels = numpy.transpose(numpy.flipud(rgb_pixels), axes = (1, 0, 2))
            lab_pixels = numpy.transpose(numpy.flipud(lab_pixels), axes = (1, 0, 2))
    elif(start_point == 'dr' or start_point == 'DR') :
        if(orientation == 'RLU'or orientation == 'rlu') :
            rgb_pixels = numpy.flipud(numpy.fliplr(rgb_pixels))
            lab_pixels = numpy.flipud(numpy.fliplr(lab_pixels))
        elif(orientation == 'URL' or orientation == 'url') :
            rgb_pixels = numpy.transpose(numpy.flipud(numpy.fliplr(rgb_pixels)), axes = (1, 0, 2))
            lab_pixels = numpy.transpose(numpy.flipud(numpy.fliplr(lab_pixels)), axes = (1, 0, 2))
    else :
        print("Incorrect Input")
        sys.exit()

    return rgb_pixels, lab_pixels

def variation_histogram (lab_pixels) :
    start_time = time.time()
    columns = numpy.transpose(lab_pixels, axes = (1, 0, 2))
    pool = ThreadPool(8)
    hor_results = pool.map(count_regions, lab_pixels)
    ver_results = pool.map(count_regions, columns)
    pool.close()
    pool.join()
    final_time = time.time()
    print("Time to calculate horizontal and vertical regions: " + str(final_time-start_time))
    start_time = time.time()
    hor_avg = sum(hor_results) / lab_pixels.shape[0]
    hor_percentage = (hor_avg * 100) / lab_pixels.shape[1]
    ver_avg = sum(ver_results) / lab_pixels.shape[1]
    ver_percentage = (ver_avg * 100) / lab_pixels.shape[0]
    avg_size = (hor_percentage + ver_percentage) / 2
    final_time = time.time()
    print("Time to calculate avg size: " + str(final_time - start_time))
    return avg_size

def count_regions (line) :
    number = 1
    for i in range (1, len(line)) :
        current = line[i]
        previous = line[i-1]

        if (color.deltaE_ciede2000(current, previous) >= 25) :
            number += 1
    return len(line) / number

def draw_variation_histogram(variations) :

    counter = []
    differences = []
    for difference in sorted(variations.keys()) :
        count = variations[difference]
        differences.append(difference)
        counter.append(count)

    ax = plt.subplot()
    width = 1/1.5
    bars = ax.bar(differences, counter, width, color='blue')
    #plt.bar(differences, counter, width, color='blue')

    #ax.set_axis_bgcolor('black')
    ax.set_ylabel("Count")
    ax.set_title("Color Differences")

    for bar in bars :
        height = bar.get_height()
        ax.text(bar.get_x() + bar.get_width()/2, 1.05*height, '%d' % int(height), ha = 'center', va = 'bottom')

   # plt.axis([0, max(differences), 0, max(counter)/3])
    plt.show()

def image_temperature (rgb_color_count) :

    temps = []
    sum = 0
    total = 0
    light_sum = 0
    sat_sum = 0
    for rgb, count in rgb_color_count.items() :
        hsl = color_conversions.pixel_rgb2hsl(rgb[0], rgb[1], rgb[2])
        temp = calculate_temp_value(hsl[0], hsl[1], hsl[2])

        temps.append((count, temp))
        # print(count)
        light_sum += hsl[2] * count
        sat_sum += hsl[1] * count
        # print("Hue: " + str(hsl[0]))
        # print("Temp: " + str(temp))
        total += count
        sum += (temp * count)

    avg = sum / total
    light_avg = int(light_sum / total)
    sat_avg = int(sat_sum / total)
    return (avg, light_avg, sat_avg)

def image_temperature_quads(quadrants) :
    total_sat = 0
    total_light = 0
    total_warmth = 0
    num_quadrants = len(quadrants)
    for i in range (0, num_quadrants) :
        quad = quadrants[i]
        total_sat += quad.get_saturation()
        total_light += quad.get_lightness()
        total_warmth += quad.get_warmth()

    return (total_warmth / num_quadrants, int(total_sat / num_quadrants), int(total_light / num_quadrants))

def calculate_temp_value (hue, sat, light) :
    temp = 0
    if (light >= 90) :
        temp = 0
    elif (light <= 5) :
        temp = 100
    elif (sat <= 10) :
        temp = 50
    elif (hue <= 180) :
        temp = (-10 * hue) / 18 + 100
    else :
        temp = (10 * hue) / 18 - 100

    return temp

def image_temp (rgb_color_count) :
    warm = 0
    cool = 0
    neutral = 0
    total = 0

    for rgb, count in rgb_color_count.items() :
        hsl = color_conversions.pixel_rgb2hsl(rgb[0], rgb[1], rgb[2])
        temp = calculate_temp_value(hsl[0], hsl[1], hsl[2])
        if (temp < 50) :
            cool += count
        elif (temp > 50) :
            warm += count
        else :
            neutral += count
        total += count

    warm_perc = int((warm / total) * 100)
    cold_perc = int((cool / total) * 100)

def aspect_ratio (width, height) :
    divisor = gcd(width, height)
    horizontal = round(width / divisor)
    vertical = round(height / divisor)
    return (horizontal, vertical)

def closest_ratio (ratio) :
    proportion = ratio[0]/ratio[1]
    lowest = sys.maxsize
    result = ()
    if(ratio[0] > ratio[1]) :
        ind1 = 1
        ind2 = 0
    else :
        ind1 = 0
        ind2 = 1

    for i in range (0, len(KNOWN_RATIOS)) :
        r = KNOWN_RATIOS[i]
        value = r[ind1] / r[ind2]
        sub = abs(value - proportion)
        if (sub < lowest) :
            lowest = sub
            result = (r[ind1], r[ind2])

    return result

def test_colors() :
    width = pixels_x * C
    height = pixels_y
    image = Image.new('RGB', (width, height))

    colors = []
    for i in range (0, C) :
        rgb = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        colors.append(rgb)

        for x in range (i * pixels_x, ((i + 1) * pixels_x)) :
            for y in range (0, height) :
                pos = (x, y)
                image.putpixel(pos, rgb)

    for j in range (1, C) :
    #     rgb1 = colors[j-1]
        rgb = colors[j]
        rgb = color_conversions.tuple_to_nparray(rgb)
        color_conversions.calculate_luminance(rgb)
    #     lab1 = color_conversions.pixel_rgb2lab(rgb1[0], rgb1[1], rgb1[2])
    #     lab2 = color_conversions.pixel_rgb2lab(rgb2[0], rgb2[1], rgb2[2])
    #     print(color.deltaE_ciede2000(lab1, lab2))

    image.show('test_colors.png')

def test_hsl() :
    colors = [(0, 0, 50), (20, 1, 50), (60, 2, 50), (100, 5, 50), (170, 10, 50), (250, 0, 50), (330, 15, 50)]

    width = pixels_x * len(colors)
    height = pixels_y
    print(width, height)
    image = Image.new('RGB', (width, height))

    for i in range (0, len(colors)) :
        hsl = colors[i]
        rgb = color_conversions.pixel_hsl2rgb(hsl[0], hsl[1], hsl[2])
        rgb = color_conversions.nparray_to_tuple(rgb)
        for x in range (i * pixels_x, ((i + 1) * pixels_x)) :
            for y in range (0, height) :
                pos = (x, y)
                image.putpixel(pos, rgb)

    for j in range (1, len(colors)) :
        hsl1 = colors[j-1]
        hsl2 = colors[j]
        lab1 = color_conversions.pixel_hsl2lab(hsl1[0], hsl1[1], hsl1[2])
        lab2 = color_conversions.pixel_hsl2lab(hsl2[0], hsl2[1], hsl2[2])
        print(lab2)
        print(color.deltaE_ciede2000(lab1, lab2))

    image.show('test_colors.png')

############# HISTOGRAM FUNCTIONS

def color_count (rgb, lab) :
    rgb_count = {}
    lab_count = {}
    for y in range (0, rgb.shape[0]) :
        for x in range (0, rgb.shape[1]) :
            rgb_pixel = rgb[y][x]
            lab_pixel = lab[y][x]
            rgb_pixel_tuple = (rgb_pixel[0], rgb_pixel[1], rgb_pixel[2])
            if (rgb_pixel_tuple in rgb_count) :
                rgb_count[rgb_pixel_tuple] += 1
            else :
                rgb_count[rgb_pixel_tuple] = 1

            lab_pixel_tuple = (lab_pixel[0], lab_pixel[1], lab_pixel[2])
            if(lab_pixel_tuple in lab_count) :
                lab_count[lab_pixel_tuple] += 1
            else :
                lab_count[lab_pixel_tuple] = 1
    return (rgb_count, lab_count)

def hsl_histogram (pixels) :
    hue_counter = numpy.zeros(360, numpy.int32)
    saturation_counter = numpy.zeros(101, numpy.int32)
    lightness_counter = numpy.zeros(101, numpy.int32)

    total_pixels = pixels.shape[0] * pixels.shape[1]

    for x in range(0, pixels.shape[0]):
        for y in range(0, pixels.shape[1]):
            pixel = pixels[x][y]
            hsl = color_conversions.pixel_rgb2hsl(pixel[0], pixel[1], pixel[2])
            hue = hsl[0]
            saturation = hsl[1]
            lightness = hsl[2]

            if (hue == 360):
                hue_counter[0] += 1
            else:
                hue_counter[hue] += 1
            saturation_counter[saturation] += 1
            lightness_counter[lightness] += 1


    print_values (hue_counter, total_pixels, "Hue")
    print_values(saturation_counter, total_pixels, "Saturation")
    print_values(lightness_counter, total_pixels, "Lightness")

    hue_histogram (hue_counter)
    saturation_histogram (saturation_counter)
    lightness_histogram (lightness_counter)

def print_values (counter, total_pixels, s) :
    max_value = 0
    min_value = 0

    for x in range (0, counter.size) :
        if (counter[x] >= counter[max_value]) :
            max_value = x
        if (counter[x] <= counter[min_value]) :
            min_value = x

    max_percentage = counter[max_value] * 100 / total_pixels
    min_percentage = counter[min_value] * 100 / total_pixels
    print(s + " max value: " + str(max_value) + " - " + str(max_percentage) + "%")
    print(s + " min value: " + str(min_value) + " - " + str(min_percentage) + "%")
    print("\n")

def hue_histogram (counter) :
    red_x = numpy.arange(16)
    orange_x = numpy.arange(15, 41)
    yellow_x = numpy.arange(40, 76)
    green_x = numpy.arange(75, 151)
    cyan_x = numpy.arange(150, 201)
    blue_x = numpy.arange(200, 271)
    magenta_x = numpy.arange(270, 331)
    red2_x = numpy.arange(330, 360)

    counter_x = 0

    red_y = numpy.empty(16, numpy.int32)
    for x in range(0, red_y.shape[0]):
        red_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    orange_y = numpy.empty(26, numpy.int32)
    for x in range(0, orange_y.shape[0]):
        orange_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    yellow_y = numpy.empty(36, numpy.int32)
    for x in range(0, yellow_y.shape[0]):
        yellow_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    green_y = numpy.empty(76, numpy.int32)
    for x in range(0, green_y.shape[0]):
        green_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    cyan_y = numpy.empty(51, numpy.int32)
    for x in range(0, cyan_y.shape[0]):
        cyan_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    blue_y = numpy.empty(71, numpy.int32)
    for x in range(0, blue_y.shape[0]):
        blue_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    magenta_y = numpy.empty(61, numpy.int32)
    for x in range(0, magenta_y.shape[0]):
        magenta_y[x] = counter[counter_x]
        counter_x += 1

    counter_x -= 1
    red2_y = numpy.empty(30, numpy.int32)
    for x in range(0, red2_y.shape[0]):
        red2_y[x] = counter[counter_x]
        counter_x += 1

    plt.plot(red_x, red_y, '#ff0000')
    plt.plot(orange_x, orange_y, '#ff8000')
    plt.plot(yellow_x, yellow_y, '#ffff00')
    plt.plot(green_x, green_y, '#00ff00')
    plt.plot(cyan_x, cyan_y, '#00ffff')
    plt.plot(blue_x, blue_y, '#0000ff')
    plt.plot(magenta_x, magenta_y, '#ff00ff')
    plt.plot(red2_x, red2_y, '#ff0000')

    ax = plt.subplot()
    ax.set_axis_bgcolor('black')
    plt.xlabel('Hue Values')
    plt.ylabel('Count')
    plt.title('Hue Histogram')
    plt.axis([0, 359, 0, counter.max() + 1000])
    plt.show()

def saturation_histogram (counter) :
    x_axis = numpy.arange(101)
    plt.plot(x_axis, counter, '#000000')
    plt.xlabel('Saturation Values')
    plt.ylabel('Count')
    plt.title('Saturation Histogram')
    plt.show()

def lightness_histogram (counter) :
    x_axis = numpy.arange(101)
    plt.plot(x_axis, counter, '#000000')
    plt.xlabel('Lightness Values')
    plt.ylabel('Count')
    plt.title('Lightness Histogram')
    plt.show()


############# AUXILIARY FUNCTIONS

def image_info (width, height) :
    print('Image Width: ', width)
    print('Image Height: ', height)
    print('Image pixels: ', height * width)

def gcd (a, b) :
    if (a == b) :
        return a
    elif (a > b) :
        return gcd(a - b, b)
    else :
        return gcd(a, b - a)

def is_prime(number) :
    if (number <= 1) :
        return False
    elif (number <= 3) :
        return True
    elif (number % 2 == 0 or number % 3 == 0) :
        return False
    i = 5
    while (i * i <= number) :
        if (number % i == 0 or number % (i + 2) == 0) :
            return False
        i += 6
    return True