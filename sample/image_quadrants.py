import numpy
import sys
from PIL import Image
import math
import image_processing

import cross_domain_mapping
import color_conversions

class Quadrant :

    def __init__ (self, id, limits) :
        self.id = id
        self.rgb_pixels = {}
        self.rgb_count = {}
        # ((Left, Right), (Up, Down))
        self.limits = limits
        self.avg_saturation = 0
        self.avg_lightness = 0
        self.warmth = 0
        self.avg_region_size = 0

    def add_pixel (self, rgb_pixel, x, y) :
        self.rgb_pixels[(x, y)] = rgb_pixel
        self.update_count(rgb_pixel)

    def update_count(self, rgb):
        rgb_tuple = (rgb[0], rgb[1], rgb[2])

        if(rgb_tuple in self.rgb_count) :
            self.rgb_count[rgb_tuple] += 1
        else :
            self.rgb_count[rgb_tuple] = 1

    def is_in_quadrant(self, x, y):
        result = False

        left = self.limits[0][0]
        right = self.limits[0][1]
        up = self.limits[1][0]
        down = self.limits[1][1]
        if (x >= left and x < right):
            if (y >= up and y < down):
                result = True
        return result

    def get_rgb_pixels(self):
        return self.rgb_pixels

    def get_rgb_count(self):
        return self.rgb_count

    def get_id(self):
        return self.id

    def get_limits(self):
        return self.limits

    def number_pixels(self):
        return len(self.rgb_pixels)
    def get_saturation(self):
        return self.avg_saturation
    def get_lightness(self):
        return self.avg_lightness
    def get_warmth(self):
        return self.warmth

    def set_saturation(self, sat):
        self.avg_saturation = sat
    def set_lightness(self, light):
        self.avg_lightness = light
    def set_warmth(self, warmth):
        self.warmth = warmth

    def calculate_avg_region_size(self, quadrant_regions):
        self.avg_region_size = self.number_pixels()/len(quadrant_regions)
    def get_avg_region_size(self):
        return self.avg_region_size


def calculate (filename, rgb, bpm) :
    width = rgb.shape[1]
    height = rgb.shape[0]
    pixels = width * height

    min_beats, max_beats = cross_domain_mapping.duration_interval(bpm)

    quad_dim, duration = determine_quadrant_dimensions(width, height, min_beats, max_beats)
    quadrants = initialize_quadrants(height, width, quad_dim[0], quad_dim[1])
    for i in range (0, len(quadrants)) :
        total_sat = 0
        total_light = 0
        total_warmth = 0
        quad = quadrants[i]
        limits = quad.get_limits()
        left = limits[0][0]
        right = limits[0][1]
        up = limits[1][0]
        down = limits[1][1]
        for y in range(up, down):
            for x in range(left, right):
                if (y >= height or x >= width):
                    continue
                pixel = rgb[y][x]
                quad.add_pixel(pixel, x, y)
                hsl = color_conversions.pixel_rgb2hsl(pixel[0], pixel[1], pixel[2])
                total_sat += hsl[1]
                total_light += hsl[2]
                total_warmth += image_processing.calculate_temp_value(hsl[0], hsl[1], hsl[2])

        quad.set_saturation(total_sat / len(quad.get_rgb_pixels()))
        quad.set_lightness(total_light / len(quad.get_rgb_pixels()))
        quad.set_warmth(total_warmth / len(quad.get_rgb_pixels()))
    show_quadrants(filename, quadrants, width, height)
    return (sorted(quadrants, key = lambda x: x.id), duration)

def initialize_quadrants (height, width, quad_width, quad_height) :
    print("Quad Width: " + str(quad_width))
    print("Quad Height: " + str(quad_height))
    vertical = int(height / quad_height)
    horizontal = int(width / quad_width)
    quadrants = []
    id = 0
    for i in range(0, vertical):
        for j in range(0, horizontal):
            limits = generate_limits(i, j, quad_width, quad_height)
            quad = Quadrant(id, limits)
            quadrants.append(quad)
            id += 1
    return quadrants

def generate_limits (i, j, quad_width, quad_height) :

    left = j * quad_width
    right = left + quad_width
    up = i * quad_height
    down = up + quad_height
    return ((left, right), (up, down))

def determine_quadrant_dimensions (width, height, min_beats, max_beats) :
    image_area = width * height
    width_divisors = calculate_divisors(width)
    height_divisors = calculate_divisors(height)
    current_sol = ()
    min_dim_distance = sys.maxsize
    for i in range (len(width_divisors) - 1, 0, -1) :
        for j in range (len(height_divisors) - 1, 0, -1) :

            quad_width = width_divisors[i]
            quad_height = height_divisors[j]
            if(quad_height > quad_width) :
                continue
            dim_distance = abs(quad_width-quad_height)
            area = quad_width * quad_height
            number = int(image_area / area)
            duration = number * 4
            if (dim_distance < min_dim_distance and duration >= min_beats and duration <= max_beats) :
                current_sol = ((quad_width, quad_height), number)

    return (current_sol[0], current_sol[1] * 4)

def calculate_divisors (number) :
    primes = sieve(number)
    factors = prime_factors(number, primes)
    divisors = set_divisors(1, 0, [], factors)
    print(sorted(divisors))
    return sorted(divisors)

def sieve (number) :
    result = numpy.full(number + 1, True, bool)
    i = 2
    while (i <= math.sqrt(number)) :
        if (result[i]) :
            j = int(math.pow(i, 2))
            while (j <= number) :
                result[j] = False
                j += i
        i += 1
    primes = []
    for i in range (2, result.shape[0]) :
        if(result[i]) :
            primes.append(i)
    return primes

def prime_factors (number, primes) :
    factors = []
    i = 0
    while (primes[i] <= math.sqrt(number)) :
        exponent = 0
        prime = primes[i]
        while (number % prime == 0) :
            exponent += 1
            number = number / prime
        if (exponent > 0) :
            factors.append((prime, exponent))
        i+=1
    if (number >= 2) :
        factors.append((number, 1))
    return factors

def set_divisors (n, i, divisors, factors) :
    for j in range (i, len(factors)) :
        x = factors[j][0] * n
        for k in range (0, factors[j][1]) :
            divisors.append(int(x))
            divisors = set_divisors(x, j+1, divisors, factors)
            x *= factors[j][0]
    return divisors

def pair_divisors (divisors) :
    divisor_pairs = []
    size = len(divisors)
    half = math.ceil(size / 2)
    for i in range (0, half+1) :
        div1 = divisors[i]
        div2 = divisors[size-1-i]
        divisor_pairs.append((div1, div2))
    return divisor_pairs

def show_quadrants(filename, quadrants, width, height):

    for i in range(0, len(quadrants)):
        quad = quadrants[i]
        image = Image.new('RGB', (width, height))
        pixels = quad.get_rgb_pixels()
        for pos, pixel in pixels.items():
            image.putpixel(pos, (pixel[0], pixel[1], pixel[2]))
        image.save('quadrants/' + filename + '-quadrant_' + str(quad.get_id()) + '.png')
