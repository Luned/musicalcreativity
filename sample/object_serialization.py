import _pickle as pickle
import time

def dump_to_file (obj, name) :
    start_time = time.time()
    with open("class_dumps/"+name, 'wb') as output:
        pickler = pickle.Pickler(output, -1)
        pickler.dump(obj)
    finish_time = time.time()
    print("time to dump to file: " + str(finish_time-start_time) + " seconds")


def load_from_file (name) :
    start_time = time.time()

    with open("class_dumps/"+name, 'rb') as input:
        object = pickle.load(input)

    finish_time = time.time()
    print("time to load from file: " + str(finish_time - start_time)+ " seconds")
    return object

