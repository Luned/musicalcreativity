import image_quadrants
import cross_domain_mapping
import color_conversions
import composer
import image_processing
import random
import operator
import numpy

#Min percentage = 0.0625 (1/4 beat)
MIN_PERC = 0.0625
MIN_BEAT = 1/4

MAX_CHORDS = 4
MIN_PRESENCE = 0.01


class NoteRegion:

    def __init__ (self, note, octave):
        self.note = note
        self.octave = octave
        self.num_notes = 1

    def get_note(self):
        return self.note

    def get_octave(self):
        return self.octave

    def get_number_notes(self):
        return self.num_notes

    def add_note(self, count):
        self.num_notes += count

    def is_equal(self, note, octave):
        return (note == self.note and octave == self.octave)


def calculate(quadrants, song):
    num_quad = len(quadrants)
    quad_start = 0
    for i in range (0, num_quad) :
        quad = quadrants[i]
        quad_size = quad.number_pixels()
        regions = create_quadrant_regions(song, quad)
        add_notes_to_song(song, quad_start, regions, quad_size)
        quad_start += 4


def create_quadrant_regions(song, quadrant):
    regions = []
    pixel_count = quadrant.get_rgb_count()
    for rgb, count in pixel_count.items():
        added = False
        note, octave, volume = generate_note(rgb)

        if(song.get_type() != 'raw'):
            octave = song.get_octave()

        for i in range (0, len(regions)):
            reg = regions[i]
            if (reg.is_equal(note, octave)):
                reg.add_note(count)
                added = True
                break

        if(not added) :
            new_region = NoteRegion(note, octave)
            regions.insert(0, new_region)

    return regions

def generate_note(rgb):
    hsl = color_conversions.pixel_rgb2hsl (rgb[0], rgb[1], rgb[2])
    note = cross_domain_mapping.hue_conversion_function(hsl[0])
    volume = cross_domain_mapping.saturation_conversion(hsl[1])
    octave = cross_domain_mapping.lightness_conversion(hsl[2])

    return (note, octave, volume)

def add_notes_to_song (song, start_quad, quad_regions, quad_size):
    random.shuffle(quad_regions)
    start = start_quad
    total_duration = 0
    for i in range (0, len(quad_regions)) :
        region = quad_regions[i]
        num_notes = region.get_number_notes()
        if (num_notes / quad_size < 0.05):
            continue
        beats = round((num_notes * 4) / quad_size, 2)
        if (beats >= MIN_BEAT):
            note = region.get_note()
            octave = region.get_octave()
            volume = song.get_avg_volume()
            duration = convert_duration(beats)

            if (duration + total_duration > 4):
                duration = 4 - total_duration

            new_note = composer.Note(note + octave, start, duration, volume)
            song.add_note(new_note)
            start += duration
            total_duration += duration
        else:
            start += MIN_BEAT
            total_duration += MIN_BEAT

def convert_duration (duration):
    if (duration <= 3/8):
        return 1/4
    elif (duration <= 3/4):
        return 1/2
    elif(duration <= 3/2):
        return 1
    elif(duration <= 3):
        return 2
    else:
        return 4

### Chords
def calculate_chords (raw_song, harmonized_song, quadrants, warmness) :
    harm_chords = {}
    volume_total = 0
    for i in range(0, len(quadrants)):
        quadrant = quadrants[i]
        quad_count = quadrant.get_rgb_count()
        sorted_count = sorted(quad_count.items(), key=operator.itemgetter(1))
        highest_count = sorted_count[len(sorted_count) - 1]
        rgb = highest_count[0]
        add_raw_chord(quadrant, raw_song, rgb)

        volume_total = count_harmonized_chords(harm_chords, rgb, volume_total)

    add_harmonized_chord(harm_chords, harmonized_song, len(quadrants), volume_total)

def add_harmonized_chord(harm_chords, harmonized_song, total_chords, volume_total):
    volume_avg = int(volume_total / total_chords)
    sorted_chords = sorted(harm_chords.items(), key=operator.itemgetter(1))
    used_chords = []
    count_sum = 0
    for i in range(0, MAX_CHORDS):
        current = sorted_chords[len(sorted_chords) - 1]
        note = current[0]
        count = current[1][0]
        warmth = current[1][1]
        perc = round(count / total_chords, 2)

        if (perc >= MIN_PRESENCE):
            if (warmth > 0.5):
                is_major = True
            else:
                is_major = False

            chord = composer.Chord(note, is_major, count, harmonized_song.get_octave(), volume_avg)
            used_chords.append(chord)
            count_sum += count
        sorted_chords.remove(current)
        if (len(sorted_chords) == 0):
            break
    chord_repetition(harmonized_song, used_chords, count_sum, total_chords)

def count_harmonized_chords(harm_chords, rgb, volume_total):
    hsl = color_conversions.pixel_rgb2hsl(rgb[0], rgb[1], rgb[2])
    warmth = image_processing.calculate_temp_value(hsl[0], hsl[1], hsl[2])
    note, octave, volume = generate_note(rgb)
    if (note in harm_chords):
        current_num = harm_chords[note][0]
        harm_chords[note] = (current_num + 1, harm_chords[note][1])
    else:
        harm_chords[note] = (1, warmth)

    volume_total += volume
    return volume_total

def add_raw_chord(quadrant, raw_song, rgb):
    note, octave, volume = generate_note(rgb)
    quad_warmth = quadrant.get_warmth()
    if (quad_warmth > 0.5):
        is_major = True
    else:
        is_major = False
    chord = composer.Chord(note, is_major, -1, octave, volume)
    raw_song.add_chord(chord)

def chord_repetition(song, chords, count_sum, total_chords) :
    percentages = []
    order = numpy.full(total_chords, 0, int)
    for i in range (0, len(chords)) :
        chord = chords[i]
        count = chord.get_count()
        percentages.append(count / count_sum)

    for i in range (0, total_chords) :
        selection = random.uniform(0, 1)
        sum = 0
        for j in range (0, len(percentages)) :
            current = percentages[j]
            if (selection < current + sum) :
                order[i] = j
                song.add_chord(chords[j])
                break
            else :
                sum += current



