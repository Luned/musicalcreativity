import composer
import random

durations = [1/4, 1/2, 1, 2, 4]

def random_music () :

    bpm = random.randint(50, 150)
    min_beats = int((120 * bpm) / 60)
    max_beats = int((180 * bpm) / 60)
    duration = generate_random_duration(min_beats, max_beats)
    rand_music = composer.Song(bpm, duration, 72, 90, 'rand')

    for i in range (0, duration, 4) :
        random_notes(rand_music


    return rand_music





def random_chords (music) :
    number = random.randrange(0, 13)
    types = ['major', 'minor']


def random_notes (music, current_start) :
    bar_time = 4
    while (bar_time > 0) :
        pitch = random.randrange(0, 14)
        octave = random.randrange(24, 109, 12)
        volume = random.randrange(30, 127)
        for i in range (0, len(durations)) :
            current = durations[i]
            if (bar_time < current) :
                break
        index = random.randrange(0, i)
        duration = durations[index]
        bar_time -= duration
        current_start += duration
        if (pitch == 13) :
            continue
        else :
            note = composer.Note(pitch+octave, current_start, duration, volume)
            music.add_note(note)


def generate_random_duration (min_beats, max_beats) :
    remainder = min_beats % 4
    if (remainder == 0) :
        duration = random.randrange(min_beats, max_beats+1, 4)
    else :
        duration = random.randrange(min_beats + (4-remainder), max_beats+1, 4)
    return duration