try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Computational Creativity Project',
    'author': 'Joana Teixeira',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'joanbteixeira@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['numpy'],
    'scripts': [],
    'name': 'MusicalCreativity'
}

setup(**config)